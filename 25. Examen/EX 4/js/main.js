'use strict';

/********VARIABLES  **************/
/** @var {nodeList}  ciblage de tous les lien a du nav*/
let targetA
/** @var {nodeList}  ciblage de tous les sections*/
let targetSection

/**********FUNCTIONS *************/
function changement (){
    targetA=document.querySelectorAll('nav a');
        console.log(targetA);
    targetSection=document.querySelectorAll('section');
        console.log(targetSection);
    for(let i =0; i<targetA.length; i++){
        targetA[i].addEventListener('click', event=>{
            targetA.forEach(element => {
                element.classList.remove('current');
            });
            targetSection.forEach(element => {
                element.classList.remove('current');
            });
            targetA[i].classList.toggle('current');
            targetSection[i].classList.toggle('current');
        });      
    };
};
/**********PROGRAM ***************/

// 1ere étape : cibler la fenêtre
window.addEventListener('DOMContentLoaded', ()=>{
    console.log("DOM entièrement chargé et analysé");

//2eme étape on cible tous les éléments, on enlève la class "current", on cible l'élement cliqué et on lui ajoute à lui et à sa section la classe "current"
    changement();
});