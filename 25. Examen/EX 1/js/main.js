'use strict';

/********VARIABLES  **************/
/* @var {string} affichage dans le document html*/
let e;
/**********FUNCTIONS *************/

function writeHello (e){
    document.write(e);
}
/**********PROGRAM ***************/

// 1ere étape : cibler la fenêtre
window.addEventListener('DOMContentLoaded', ()=>{
    console.log("DOM entièrement chargé et analysé");

    // on affiche hello
    writeHello('Bonjour');
});
